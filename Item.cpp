#include "Item.h"



Item::Item(std::string _name, std::string _serialNumber, double _unitPrice)
{
	this->_name = _name;
	this->_serialNumber = _serialNumber;
	this->_unitPrice = _unitPrice;
	this->_count = 1;

}

Item::~Item()
{
}

double Item::totalPrice() const
{

	return this->_count * this->_unitPrice;
}

bool Item::operator<(const Item& other) const
{
	return (this->_serialNumber < other._serialNumber);
}

bool Item::operator>(const Item& other) const
{
	return (this->_serialNumber > other._serialNumber);
}

bool Item::operator==(const Item& other) const
{
	return (this->_serialNumber == other._serialNumber);
}

std::string Item::getName()
{
	return this->_name;
}

int Item::getCount()
{
	return this->_count;
}

std::string Item::getSerialNumber()
{
	return std::string();
}

void Item::setName(std::string _name)
{
}

void Item::changeCount()
{
	this->_count;
}

void Item::setSerialNumber(std::string _serialNumber)
{
}
