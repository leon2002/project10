#include "Customer.h"

Customer::Customer(std::string _name)
{
	this->_name = _name;

}

Customer::Customer()
{
}

double Customer::totalSum() const
{
	return 0.0;
}

void Customer::addItem(Item item)
{
	if (this->_items.count(item) == 1) {
		// exists
		std::cout << "item is already exists in set!" << std::endl;
	}
	else {
		// doesn't exist
		this->_items.insert(item);
	}
}

void Customer::removeItem(Item item)
{
	if (this->_items.count(item) == 1) {
		// exists
		this->_items.erase(item);
	}
	else {
		// doesn't exist
		std::cout << "item isn't exists in set!" << std::endl;
	}
}

std::string Customer::getName()
{
	return this->_name;
}

std::set<Item> Customer::getItems()
{
	return this->_items;
}

/*
Item Customer::getItem(){
	for (std::set<Item>::iterator item = this->_items.begin(); item != this->_items.end(); item++) {
		if ((*item)->getName() == _name) {
			return item;
		}
	}

}
*/