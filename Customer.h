#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string _name);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set


	//get and set functions
	std::string getName();
	std::set<Item> getItems();
	// Item getItem(std::string _name);


private:
	std::string _name;
	std::set<Item> _items;


};
